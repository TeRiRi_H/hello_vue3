import mitt from "mitt";

const emitter= mitt()

// 绑定事件
emitter.on('test1',()=>{
    console.log('调用test1')
})

emitter.on('test2',()=>{
    console.log('test2调用')
})

// 触发事件

setTimeout(() => {
    emitter.emit('test1')
    emitter.emit('test2')
},3000);



export default emitter